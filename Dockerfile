FROM tomcat

MAINTAINER mdmerton

COPY target/hello-scalatra.war /usr/local/tomcat/webapps/hello-scalatra.war

EXPOSE 8080

CMD ["catalina.sh", "run"]

